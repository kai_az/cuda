new WOW().init();
$(document).ready(function(){
 
  $('#touch_menu').click(function(){  
      $('.navigatiom_header').stop().slideToggle(500); 
        if ($(this).hasClass('not-active')) {
        $(this).addClass('is-active').removeClass('not-active');
   }
   else{
      setTimeout(function(){
      $('.is-active').addClass('not-active').removeClass('is-active')}
      ,500);   
      }  
  });


$(".navigation").on("click", "a", function(event) {
    event.preventDefault();
    var id = $(this).attr('href'),
    top = $(id).offset().top;
    $('body,html').animate ({ scrollTop: top }, 1500);
});

  $('.dial-1').knob({
    'fgColor': '#30bae7',
    'width': 160,
    'height': 160,
    'inputColor': '#3c4761',
    'font': 'Titillium Web',
    'fontWeight': 300
   });

   $('.dial-2').knob({
    'fgColor': '#d74680',
    'width': 160,
    'height': 160,
    'inputColor': '#3c4761',
    'font': 'Titillium Web',
    'fontWeight': 300,
    
   });

   $('.dial-3').knob({
    'fgColor': '#15c7a8',
    'width': 160,
    'height': 160,
    'inputColor': '#3c4761',
    'font': 'Titillium Web',
    'fontWeight': 300
   });
  
  $('.dial-4').knob({
    'fgColor': '#eb7d4b',
    'width': 160,
    'height': 160,
    'inputColor': '#3c4761',
    'font': 'Titillium Web',
    'fontWeight': 300
   });
  
  /*knobs*/
  
  
  /*tabs*/
  $(function () {
    var tabContainers = $('.tabs_block > div'); 
    tabContainers.hide().filter(':first').show(); 
    $('div.tabs_block ul.tabs a').click(function () {
        tabContainers.hide(); 
        tabContainers.filter(this.hash).show(); 
        $('div.tabs_block ul.tabs a').removeClass('selected'); 
        $(this).addClass('selected'); 
        return false;
    }).filter(':first').click();
	});
});